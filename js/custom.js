 // slick-slider
$(document).ready(function(){
	// slider1
	$('.slider').slick({
		cssEase: 'linear',
		arrows: false,
		dots: true,
		infinite: true,
	});
	// slider-2(reason-section)
	$('.slidertwo').slick({
		cssEase: 'linear',
		arrows: false,
		dots: true,
		infinite: true,
	});
	// slider-3(testimonial)
	$('.testimonialslider').slick({
		cssEase: 'linear',
		arrows: false,
		dots: true,
		infinite: true,
	});
});
// navigation-dropdown
jQuery(document).ready(function ($) {
$('.navbar .dropdown').hover(function() {
    $(this).addClass('extra-nav-class').find('.dropdown-menu').first().stop(true, true).delay(300).slideDown();
}, function() {
    var na = $(this)
    na.find('.dropdown-menu').first().stop(true, true).delay(100).slideUp('fast', function(){ na.removeClass('extra-nav-class') })
});
}); 
